Lenovo Ideapad 310-15IKB-80TV00QQMJ (Ideapad 310 Series)
macOS Mojave 10.15.4

Configuration Tools:
Clover Configurator - https://mackie100projects.altervista.org/download-clover-configurator/
Hackintool - https://github.com/headkaze/Hackintool

Stock specification
=============================================
Processor: Intel Core i5-7200U
Graphics adapter: NVIDIA GeForce 920MX - 2048 MB
Memory: 4096 MB DDR4-2133
Display: 15.6 inch 16:9, 1920 x 1080 pixel 141 PPI, BOE NT156FHM-N31, TN LED, glossy: yes
Mainboard: Intel Kaby Lake-U Premium PCH, Lenovo Torronto 5C2
Storage:  1 TB 5400rpm HDD
Connections: 2 USB 2.0, 1 USB 3.0 / 3.1 Gen1, 1 VGA, 1 HDMI, 1 DisplayPort, 1 Kensington Lock
Audio Connections: 1.5
Card Reader: 4-in-1 SD
Networking: Realtek RTL8168/8111 Gigabit-LAN (10/100/1000MBit), Intel Dual Band Wireless-AC 3165 (a/b/g/n/ac), Bluetooth 4.0
Optical drive: DVD +/- RW Double Layer
Battery: 30 Wh Lithium-Polymer
Camera: Webcam: 720p
Additional features: 
Speakers: Stereo
Keyboard: Chiclet, 6 row, Numblock

My specification
=============================================
Memory: 12288 MB
Storage:  128 GB SSD

Working Features
=============================================
- Battery
- HDMI with sounds

Kext
=============================================
- AppleALC.kext
- HibernationFixup.kext
- Lilu.kext
- NoTouchID.kext
- RealtekRTL8111.kext
- USBInjectAll.kext
- VirtualSMC.kext
- VoodooInput.kext
- VoodooPS2Controller.kext
- VoodooTSCSync.kext
- WhateverGreen.kext

To be solve
=============================================
- HDMI disconnect sometimes, auto connect
- Nvidia driver (Waiting Nvidia Web Driver)
- Clock won't update after wake up from sleeps
